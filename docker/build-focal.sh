#!/usr/bin/env bash

DOCKERFILE=$1

# Options used:
#   -t : set Docker tag
#   -f : Docker file to be used to build the image
sudo docker build . \
	-t $DOCKERFILE:latest \
	-f $DOCKERFILE \
	# --no-cache # Try in case something goes wrong with package updates. 
